from virgotools import *
import numpy as np
import math as mt
#import control as ctrl
import matplotlib.pyplot as plt
#from scipy.signal import *
import scipy.signal as sig
import scipy.io

#load data
ind = 'V1:META_ITF_LOCK_index'
gps = utc2gps(2023,2,19,16,59,50)
dur = 6*60*60
indd = getChannel(ind, gps, dur)
t_ind = indd.time
d_ind = indd.data

#find the start and stop time stamp for the state we want, here it is state 59
t_start = []
t_stop = []
for i in range(len(t_ind)):
    if i == 0 and d_ind[0] == 59:
        t_start.append(0)
    if d_ind[i] < 59 and d_ind[i+1] == 59:
        t_start.append(i+1)
    if d_ind[i-1] == 59 and d_ind[i] > 59:
        t_stop.append(i-1)
    if i == len(t_ind)-1 and d_ind[-1] == 59:
        t_stop.append(len(t_ind)-1)

#plot all states and check the index finding result  
plt.plot(np.array(t_ind)/60.0, d_ind, label = 'all state indexes')
plt.plot(np.array(t_start)/60.0, d_ind[t_start], "xr", label = 'start of Locking arms beat DRMI 1F')
plt.plot(np.array(t_stop)/60.0, d_ind[t_stop], "xb", label = 'end of Locking arms beat DRMI 1F')
plt.xlabel('Time [minute]')
plt.ylabel('META_ITF_LOCK_index')
plt.title('Interferometer state on 20220219 starting from 17:00, 6 hours')
plt.legend()
plt.grid()

#save the start and stop time stamp for the state we want
np.savetxt('t_start.txt', t_start)
np.savetxt('t_stop.txt', t_stop)
